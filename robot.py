class Mars:
    max_len_command = 100

    def __init__(self):
        """
        :param position_def:
        :param position_now:
        """
        self.grid = None
        self.departure = []

    def check_grid(self, command):
        grid = self.parse_first(command)
        if any([x < 1 or x > 50 for x in grid]):
            return False
        return True

    def parse_first_command(self, command):
        self.grid = self.parse_first(command)

    def parse_first(self, command):
        grid = [int(i) for i in command.split(' ')]
        return grid

    def action(self, commands, *args, **kwargs):
        command1, command2 = commands.split('\n')

        x, y, direction = Bot.parse_start_point(command1)
        flag = self.check(x, y, direction)
        if not flag:
            print("Error")
            return

        if len(command2) > self.max_len_command:
            print("Max len command")
            return

        bot = Bot(command1, self)
        result = bot.run_command(command2)

    def check(self, x, y, direction):
        if x >= self.grid[0] or x < 0:
            return False
        if y >= self.grid[1] or y < 0:
            return False
        return True

    def check_departure(self, position):
        return position in self.departure

    def add_departure(self, position):
        self.departure.append(position)


class Bot:

    direction_key = {
        'N': 0,
        'E': 1,
        'S': 2,
        'W': 3,
    }

    direction_list = ['N', 'E', 'S', 'W']

    direction_step = {
        0: {'x': 0, 'y': 1},
        1: {'x': 1, 'y': 0},
        2: {'x': 0, 'y': -1},
        3: {'x': -1, 'y': 0},
    }

    def __init__(self, start_point, planet):
        self.frag = False
        self.x, self.y, self.direction = self.parse_start_point(start_point)
        self.planet = planet

    @classmethod
    def parse_start_point(cls, start_point):
        x, y, direction = start_point.split(' ')
        x = int(x)
        y = int(y)
        direction = cls.direction_key[direction]
        return x, y, direction

    def run_command(self, commands):
        if self.frag:
            print('LOST')
            return None
        for command in commands:
            curent_point = self.get_curent_point()
            func_command = getattr(self, 'command_{}'.format(command))
            func_command()
            status = self.planet.check(self.x, self.y, self.direction)
            if not status:
                self.frag = True
                self.planet.add_departure(curent_point)
                print(curent_point, 'LOST')
                return None
        result = self.get_curent_point()
        print(result)
        return result

    def command_L(self):
        self.direction = (self.direction - 1) % 4

    def command_R(self):
        self.direction = (self.direction + 1) % 4

    def command_F(self):
        check_departure_str = self.get_curent_point()
        status = self.planet.check_departure(check_departure_str)
        if not status:
            step = self.direction_step[self.direction]
            self.y += step['y']
            self.x += step['x']

    def get_curent_point(self):
        curent_point = ' '.join([str(i) for i in [self.x, self.y, self.direction_list[self.direction]]])
        return curent_point


    """

    TESTING
    """


if __name__ == "__main__":
    first_command = '10 10'

    commands_bot = [
        '1 1 E\nRFRFRFRF',
        '8 8 E\nFFFFFF',
        # From exemple
        '1 1 E\nRFRFRFRF',
        '3 2 N\nFRRFLLFFRRFLL',
        '0 3 W\nLLFFFLFLFL',
    ]

    mars = Mars()
    if not mars.check_grid(first_command):
        raise ValueError("")

    mars.parse_first_command(first_command)

    for commands in commands_bot:
        print("Commads:")
        print(commands)
        print("Result:")
        mars.action(commands)
        print()


